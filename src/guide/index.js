define(['text!./template.html'], function(template) {
    'use strict';

    return window.guide = {
        show: function() {
            songList.positionPageChange(0);
            keyBoard.clearSearchValue();
            document.querySelector(".primary").innerHTML = template;
        }
    }
});