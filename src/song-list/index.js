define(['text!./template.html', 'src/helper/mySocket', '../utils/utils.js', '../helper/keyEvent', './list-item/index', '../utils/slug'], 
    function(template, socket, utils, keyEvent, listItem, slug){
        var startIndex = 0;
        var currentAction = 0;
        var oldGap = 0;
        var gap = 0;
        var storeBookMark = null;
        var storeSequence = null;
    function keyHandle(event) {
        if(globalVariable.allowAll != true) return;
        switch(event.keyCode) {
            case keyCodes.CODE_KEY_UP:
                if(startIndex == 0){
                    focusSong(startIndex);
                    focusManager.active(keyBoard);
                }else {
                    focusSong(startIndex - 1);
                }
                break;
            case keyCodes.CODE_KEY_DOWN:
                focusSong(startIndex + 1);
                break;
            case keyCodes.CODE_KEY_RIGHT:
                if(globalVariable.itemEnter != null){
                    focusUserAction(currentAction + 1);
                }
                break;
            case keyCodes.CODE_KEY_LEFT:
                if(globalVariable.itemEnter != null){
                    focusUserAction(currentAction - 1);
                }else {
                    focusManager.active(leftMenu);
                }
                break;
            case keyCodes.CODE_KEY_ENTER:
                if(globalVariable.itemEnter == null){
                    selectedSong();
                    focusUserAction(currentAction);
                }else {
                    globalVariable.userAction.click();
                    focusSong(startIndex);
                }
                break;
        }
    }

    function addBookMarkClass(idSong) {
        if(document.getElementById(idSong)){            
            var detailUI = document.getElementById(idSong).childNodes[utils.INDEX_0];          
            var actionUI = detailUI.childNodes[utils.INDEX_2];
            var bookMarkUI = actionUI.childNodes[utils.INDEX_1];
            if(bookMarkUI.classList == "add-like inactive"){
                bookMarkUI.classList.add("active");
            }
        }
    }

    function removeBookMarkClass(idSong) {
        if(document.getElementById(idSong)){
            var detailUI = document.getElementById(idSong).childNodes[utils.INDEX_0];
            var actionUI = detailUI.childNodes[utils.INDEX_2];
            var bookMarkUI = actionUI.childNodes[utils.INDEX_1];
            if(bookMarkUI.classList == "add-like active"){
                bookMarkUI.classList.remove("active");
            }
        }
    }

    function removeSongElement(idSong) {
        if(document.getElementById(idSong)){
            document.getElementById(idSong).remove();
            checkFocusItem();
        }
    }

    function updateElementSequence(idSong, data){
        if(document.getElementById(idSong)){
            renderSequence(data);
            checkFocusItem();
        }
    }

    function addBookMarkElement(){
        var strSearch = keyBoard.getStringSearch();
        if(strSearch == ""){
            renderBookMark(storeBookMark);
        }else {
            searchListBookMark(strSearch);
        }
        checkFocusItem();
    }

    function addSequenceElement(){
        var strSearch = keyBoard.getStringSearch();
        if(strSearch == ""){
            renderSequence(storeSequence);
        }else {
            searchListSequence(strSearch);
        }
        checkFocusItem();
    }

    function checkFocusItem() {
        if(globalVariable.storeComp.compName != "leftMenu"){
            focusItemOrChangeComp();
        }
    }

    function focusItemOrChangeComp(){
        if(globalVariable.allowRight == true){
            if(document.querySelectorAll(".song-item").length > 0){
                resetAll();
                focusSong(startIndex);
            }else {
                focusManager.active(leftMenu);
                globalVariable.allowRight = false;
            }
        }
    }

    function removeSequenceClass(idSong) {
        if(document.getElementById(idSong)){
            var detailUI = document.getElementById(idSong).childNodes[utils.INDEX_0];
            var actionUI = detailUI.childNodes[utils.INDEX_2];
            var sequenceUI = actionUI.childNodes[utils.INDEX_0];                
            if(sequenceUI.classList.className == "add-list inactive active"){
                sequenceUI.classList.remove("active");
            }
            if(sequenceUI.classList == "add-list inactive active"){
                sequenceUI.classList.remove("active");
            }
        }
    }

    function addSequenceClass(idSong) {
        if(document.getElementById(idSong)){
            var detailUI = document.getElementById(idSong).childNodes[utils.INDEX_0];
            var actionUI = detailUI.childNodes[utils.INDEX_2];
            var sequenceUI = actionUI.childNodes[utils.INDEX_0];                
            if(sequenceUI.classList.className == "add-list inactive"){
                sequenceUI.classList.add("active");
            }
            if(sequenceUI.classList == "add-list inactive"){
                sequenceUI.classList.add("active");
            }
        }
    }

    function selectedSong (){
        globalVariable.itemFocus.classList.add("active");
        var userActionDOM = globalVariable.itemFocus.children[utils.INDEX_1];

        var topPosition = document.getElementsByClassName("song-list")[utils.INDEX_0].offsetTop;
        addOn = userActionDOM.offsetTop;      
        var mainListDOM = document.getElementsByClassName("main-list")[utils.INDEX_0];      
        var current = parseInt(mainListDOM.style.marginTop, 10);            
        if(isNaN(current)){
            current = 0;
        } 
        mainListDOM.style.marginTop = (current - addOn) + "px";
        var itemPosition = globalVariable.itemFocus.offsetTop;
        if(topPosition > itemPosition){
            mainListDOM.style.marginTop = current + "px";
        }

        globalVariable.itemEnter = globalVariable.itemFocus;
    }

    function contentSearchYouTube(){
        socket.on("listKaraoke", function (response){
            if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                renderTrending(response.data.listData.listKaraoke.items);
            }
        });
    }

    function focusUserAction(index){
        var userActionDOM = globalVariable.itemEnter.children[utils.INDEX_1];            
        var nextActionDOM = userActionDOM.children[index];
        if(nextActionDOM){
            userActionDOM.children[currentAction].classList.remove("active");
            userActionDOM.children[index].classList.add("active");
            globalVariable.userAction = userActionDOM.children[index];
            currentAction = index;
        }            
    }

    function focusSong (index){
        var bottomPosition = document.getElementsByClassName("primary")[utils.INDEX_0].offsetHeight;
        var topPosition = document.getElementsByClassName("song-list")[utils.INDEX_0].offsetTop;
        var mainListDOM = document.getElementsByClassName("main-list")[utils.INDEX_0];
        var nextChildrenDOM = mainListDOM.children[index];

        if(globalVariable.itemEnter != null && nextChildrenDOM){
            var userActionDOM = globalVariable.itemEnter.children[utils.INDEX_1];
            userActionDOM.children[currentAction].classList.remove("active");
            currentAction = 0;

            globalVariable.itemEnter.classList.remove("active");
            nextChildrenDOM.classList.add("focused");
            globalVariable.itemEnter = null;
            var current = parseInt(mainListDOM.style.marginTop, 10);
            if(index == 0){
                mainListDOM.style.marginTop = 0 + "px";
            }else if(current != 0){
                mainListDOM.style.marginTop = (current + addOn) + "px";
            }
        }        
        
        if(nextChildrenDOM) {
            globalVariable.itemFocus = mainListDOM.children[index];
            var itemsPosition = nextChildrenDOM.offsetTop;
            if(itemsPosition > bottomPosition){
                gap = bottomPosition - itemsPosition;
                mainListDOM.style.marginTop = (gap - oldGap) + "px";
                oldGap = oldGap - gap;
            }
            if(topPosition > itemsPosition){
                var current = parseInt(mainListDOM.style.marginTop, 10);
                mainListDOM.style.marginTop = current + (topPosition - itemsPosition)+ "px";
                oldGap = oldGap - (topPosition - itemsPosition);
            }
            mainListDOM.children[startIndex].classList.remove("focused");
            nextChildrenDOM.classList.add("focused");
            startIndex = index;
        }
    }

    function getListBookMark() {
        socket.emit("getListBookMark");
    }

    function listenListBookMark() {
        socket.on("listBookMark", function (response) {
            if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                renderBookMark(response.data.detail);
                storeBookMark = response.data.detail;
                socket.off("listBookMark");
            }
        });
    }

    function searchListBookMark(char) {
        foundData = [];
        for(var i = 0; i < storeBookMark.length; i++){
            var slugSongName = storeBookMark[i].videoDetail.snippet.title.latinise().toLowerCase();
            if(slugSongName.includes(char)){
                foundData.push(storeBookMark[i]);
            }
        }
        renderBookMark(foundData);
    }

    function searchListSequence(char) {
        foundData = [];
        for(var i = 0; i < storeSequence.length; i++){
            var slugSongName = storeSequence[i].videoDetail.snippet.title.latinise().toLowerCase();
            if(slugSongName.includes(char)){
                foundData.push(storeSequence[i]);
            }
        }
        renderSequence(foundData);
    }

    function updateStoreBookMark(objData) {
        if(objData.type == utils.ACTION_REMOVE && storeBookMark){
            var removeId = objData.removeDetail.videoId; 
            for(var i = 0; i < storeBookMark.length; i++) {
                if(storeBookMark[i].videoId == removeId){
                    storeBookMark.splice(i, 1);
                }
            };
        }else if(objData.type == utils.ACTION_ADD && storeBookMark){
            var addId = objData.addDetail.videoId;
            var found = false;
            for(var i = 0; i < storeBookMark.length; i++) {
                if(storeBookMark[i].videoId == addId){
                    found = true;
                    break;
                }
            };
            if(found == false){
                storeBookMark.push(objData.detail[0]);
            }
        }
    }

    function updateStoreSequence(objData) {
        if(objData.type){
            if(objData.type == utils.ACTION_REMOVE && storeSequence){
                storeSequence = objData.detail;
            }else if(objData.type == utils.ACTION_ADD && storeSequence){
                storeSequence.push(objData.detail[0]);
            }
        }else {
            storeSequence = objData.detail;
        }
        
    }


    function getListSequence() {
        socket.emit("getListSequence");
    }

    function listenListSequence(){
        socket.on("listSequence", function (response) {
            if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                renderSequence(response.data.detail);
                storeSequence = response.data.detail;
                socket.off("listSequence");
            }
        });
    }

    function getListStart(){
        socket.on("hostResponse", function (response) {
            if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                renderTrending(response.data.listKaraoke.items);
            }
        });
    };

    function getListTrending(){
        socket.emit("getList", { "keySearch":  utils.SEARCH_DEFAULT } );
    }

    function resetAll () {
        startIndex = 0;
        oldGap = 0;
        gap = 0;
    }

    function checkDataRender (data){
        if(data.length == 0) {
            globalVariable.allowRight = false;
            globalVariable.allowBottom = false;
            return;
        }else {
            globalVariable.allowRight = true;
            globalVariable.allowBottom = true;
        }
    }
    function renderTrending(data){
        console.log("log before render trending");
        checkDataRender(data);
        var strHtml = '';
        var mainList = document.querySelector('.main-list');
        if(mainList.hasChildNodes()){
            mainList.innerHTML = "";
        }
        for(var i = 0; i < data.length; i ++) {
            console.log("log render item " + i);
            var snippet = data[i].snippet;
            var item = new listItem(data[i], {
                id: data[i].id.videoId,
                songName: snippet.title,
                imageUrl: snippet.thumbnails.default.url,
                icons: [
                    {
                        class: ["add-list", (data[i].sequence === true)?"active":"inactive"]
                    },
                    {
                        class: ["add-like", (data[i].bookmarked === true)?"active":"inactive"]
                    }
                ],
                actions: [
                    {
                        class: 'play',
                        method: 'play',
                        title: 'Phát'
                    },
                    {
                        class: 'add-list',
                        method: 'addSequence',
                        title: 'Thêm danh sách chờ'
                    },
                    {
                        class: 'add-like',
                        method: 'addBookMark',
                        title: 'Thêm vào yêu thích'
                    },
                ]
            });
            mainList.appendChild(item.element);
        }
        console.log("log end render trending");
    }

    function renderBookMark(data) {
        checkDataRender(data);
        strHtml = '';
        var mainList = document.querySelector('.main-list');
        if(mainList.hasChildNodes()){
            mainList.innerHTML = "";
        }
        for(var i = 0; i < data.length; i ++){
            var snippet = data[i].videoDetail.snippet;
            var imgSrc = "";
            if(snippet.thumbnails.default){
                imgSrc = snippet.thumbnails.default.url
            }else if(snippet.thumbnails.medium) {
                imgSrc = snippet.thumbnails.medium.url
            }

            var item = new listItem(data[i].videoDetail, {
                id: data[i].videoDetail.id.videoId,
                songName: snippet.title,
                imageUrl: imgSrc,
                icons: [
                    {
                        class:["add-like","active"]
                    }
                ],
                actions: [
                    {
                        class: 'play',
                        method: 'play',
                        title: 'Phát'
                    },
                    {
                        class: 'add-list',
                        method: 'addSequence',
                        title: 'Thêm danh sách chờ'
                    },
                    {
                        class: 'remove-like',
                        method: 'removeBookMark',
                        title: 'Bỏ yêu thích'
                    },
                ]
            });
            mainList.appendChild(item.element);
        };
    }

    function renderSequence(data) {
        checkDataRender(data);
        strHtml = '';
        var mainList = document.querySelector('.main-list');
        if(mainList.hasChildNodes()){
            mainList.innerHTML = "";
        }
        for(var i = 0; i < data.length; i ++){
            var snippet = data[i].videoDetail.snippet;
            var detail = data[i].videoDetail;

            var imgSrc = "";
            if(snippet.thumbnails.default){
                imgSrc = snippet.thumbnails.default.url
            }else if(snippet.thumbnails.medium) {
                imgSrc = snippet.thumbnails.medium.url
            }

            var item = new listItem(detail, {
                id: data[i].videoDetail.id.videoId,
                songName: snippet.title,
                imageUrl: imgSrc,
                icons: [
                    {
                        class:["add-list","active"]
                    }
                ],
                actions: [
                    {
                        class: 'play',
                        method: 'playSequence',
                        title: 'Phát'
                    },
                    {
                        class: 'prior',
                        method: 'priority',
                        title: 'Ưu tiên'
                    },
                    {
                        class: 'remove-list',
                        method: 'removeSequence',
                        title: 'Bỏ danh sách chờ'
                    },
                ]
            });
            mainList.appendChild(item.element);
        };
    }
    
    function addKeyboardAndSongListDOM () {
        if(!document.querySelector(".keyboard")){
            document.querySelector(".primary").innerHTML = "<div class='keyboard'></div>";
            keyBoard.show();
        }
        if(!document.querySelector(".song-list")){
            document.querySelector(".primary").innerHTML += "<div class='song-list'></div>";
            songList.show();
        }
    }

    function positionPageChange(intPage){
        globalVariable.positionPage = intPage;
    }

    function offListenTrending() {
        socket.off("listKaraoke");
        socket.off("hostResponse");
    }
    return window.songList = {
        compName: "songList",
        show: function() {
            console.log("log show song-list");
            getListStart();
            document.querySelector('.song-list').innerHTML = template;
            contentSearchYouTube();
        },
        inactive: function() {
            var mainListDOM = document.getElementsByClassName("main-list")[utils.INDEX_0];
            if(mainListDOM && mainListDOM.children.length != 0){
                mainListDOM.children[startIndex].classList.remove("focused");
                mainListDOM.children[startIndex].classList.add("actived");
            }
        },
        focusItem:function () {
            focusSong(startIndex);
        },
        hookEvent: function(){
            keyEvent.hook(keyHandle);
        },
        reactive: function (){
            var mainListDOM = document.getElementsByClassName("main-list")[utils.INDEX_0];
            if(mainListDOM.children.length != 0){
                mainListDOM.children[startIndex].classList.remove("actived");
                focusSong(startIndex);
            }
        },
        keyHandle: function(event){
            keyHandle(event);
        },
        trendingShow: function () {
            addKeyboardAndSongListDOM();
            keyBoard.clearSearchValue();
            resetAll();
            getListTrending();
            contentSearchYouTube();
            positionPageChange(1);
            document.querySelector('.song-list').innerHTML = template;
        },
        bookMarkShow: function () {
            addKeyboardAndSongListDOM();
            keyBoard.clearSearchValue();
            resetAll();
            getListBookMark();
            listenListBookMark();
            positionPageChange(2);
            document.querySelector('.song-list').innerHTML = template;
            offListenTrending();
        },
        sequenceShow: function () {
            addKeyboardAndSongListDOM();
            keyBoard.clearSearchValue();
            resetAll();
            getListSequence();
            listenListSequence();
            positionPageChange(3);
            document.querySelector('.song-list').innerHTML = template;
            offListenTrending();
        },
        resetAll: function() {
            resetAll();
        },
        positionPageChange: function(intPage){
            positionPageChange(intPage);
        },   
        removeSequenceClass: function(idSong) {
            removeSequenceClass(idSong);
        },
        addSequenceClass: function (idSong) {
            addSequenceClass(idSong);
        },
        removeBookMarkClass: function(idSong) {
            removeBookMarkClass(idSong);
        },
        addBookMarkClass: function(idSong) {
            addBookMarkClass(idSong);
        },
        removeSongElement: function(idSong){
            removeSongElement(idSong);
        },
        addBookMarkElement: function(){
            addBookMarkElement();
        },
        addSequenceElement: function () {
            addSequenceElement();
        },
        searchBookMark: function (char) {
            searchListBookMark(char);
        },
        updateStoreBookMark: function (objData){
            updateStoreBookMark(objData);
        },
        updateStoreSequence: function (objData){
            updateStoreSequence(objData);
        },
        searchSequence: function(char) {
            searchListSequence(char);
        },
        updateElementSequence: function(idSong, data){
            updateElementSequence(idSong, data);
        }
    }
})