define(['src/helper/mySocket'], function(socket) {
    var operations = {
        play: function play (){
            socket.emit("selectVideo", { "videoId": this.videoDetail.id.videoId, "videoDetail": JSON.stringify(this.videoDetail) });
        },

        playSequence:function playSequence () {
            // video.nextSong()
            socket.emit("selectVideo", { "videoId": this.videoDetail.id.videoId, "videoDetail": JSON.stringify(this.videoDetail) });
            socket.emit("removeSequence", { "videoId": this.videoDetail.id.videoId, "position": this.videoDetail.originalPosition.toString() });
        },
    
        addBookMark: function addBookMark (){    
            socket.emit("addBookMark", { "videoId": this.videoDetail.id.videoId, "videoDetail": JSON.stringify(this.videoDetail)} );        
        },
    
        removeBookMark: function removeBookMark () {
            socket.emit("removeBookMark", { "videoId": this.videoDetail.id.videoId, "videoDetail": JSON.stringify(this.videoDetail) } );
        },
    
        addSequence: function addSequence () {
            socket.emit("addSequence", { "videoId": this.videoDetail.id.videoId, "videoDetail": JSON.stringify(this.videoDetail) });
        },
    
        removeSequence: function removeSequence () {
            socket.emit("removeSequence", { "videoId": this.videoDetail.id.videoId, "position": this.videoDetail.originalPosition.toString() });
        },
    
        priority: function priority () {
            socket.emit("priorityVideo", { "videoId": this.videoDetail.id.videoId, "position": this.videoDetail.originalPosition.toString() } );
        }
    }

    return function(itemValue, options) {
        var element = document.createElement('div');
        element.id = options.id;
        element.classList.add('song-item');

        var elDetail = document.createElement("div");
        elDetail.classList.add('details');
        var songImg = document.createElement("div");
        songImg.classList.add("song-img");
        var img = document.createElement("img");
        img.src = options.imageUrl;
        songImg.appendChild(img);
        elDetail.appendChild(songImg);
        
        var songName = document.createElement("div");
        songName.classList.add("song-name");
        var songNameText = document.createElement("p");
        songNameText.textContent = options.songName;
        songName.appendChild(songNameText);
        elDetail.appendChild(songName);

        var iconAction = document.createElement("ul");
        iconAction.classList.add("action");
        for(var i = 0; i < options.icons.length; i++){
            var iconClass = options.icons[i];
            var iconItem = document.createElement("li");
            var spanList = document.createElement("span");
            iconItem.appendChild(spanList);
            if(iconClass.class.length == 1){
                iconItem.classList.add(iconClass.class);
            }else {                
                iconItem.classList.add(iconClass.class[0]);
                iconItem.classList.add(iconClass.class[1]);
            }
            iconAction.appendChild(iconItem);
        }
        elDetail.appendChild(iconAction);
        element.appendChild(elDetail);

        var userAction = document.createElement("ul");
        userAction.classList.add("user-action");        
        for(var i = 0; i < options.actions.length; i++) {
            var action = options.actions[i];
            var act = document.createElement('li');
            act.classList.add(action.class);
            act.addEventListener('click', operations[action.method].bind({ videoDetail: itemValue }), false);
            act.innerHTML = '<span>'+ action.title +'</span>';
            userAction.appendChild(act);
        }
        element.appendChild(userAction);
        this.element = element;
    }
});