define({
    "RESPONSE_STATUS_SUCCESS": 200,
    "ACTION_ADD": "add",
    "ACTION_REMOVE": "remove",
    "SEQUENCE_ACTION_REMOVE_MOBILE":"requestRemoveSequence",
    "SEQUENCE_ACTION_ADD_MOBILE":"requestAddSequence",
    "PLATFORM": "smarttv",

    "RANGE_ID": 10,

    "VIDEO_FORMAT_MP4_HD720": "video_mp4_hd720",
    "VIDEO_FORMAT_WEBM_MEDIUM": "video_webm_medium",
    "VIDEO_FORMAT_MP4_MEDIUM": "video_mp4_medium",

    "STR_0": "0",
    "STR_1": "1",

    "INDEX_0": 0,
    "INDEX_1": 1,
    "INDEX_2": 2,

    "CHAR_INDEX_21": 21,
    "CHAR_INDEX_30": 30,

    "CHAR_RANGE_9": 9,
    "CHAR_RANGE_10": 10,

    "SEARCH_DEFAULT": "karaoke",
});