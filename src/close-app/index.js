define(function(){
    'use strict';
        var startIndex = 0;
        var storeDOM = null;
    function keyHandle(event) {        
        switch(event.keyCode) {
            case keyCodes.CODE_KEY_RIGHT:
                if(globalVariable.itemFocus != null){
                    focusCloseApp(startIndex + 1);
                }
                break;
            case keyCodes.CODE_KEY_LEFT:
                if(globalVariable.itemFocus != null){
                    focusCloseApp(startIndex - 1);
                }
                break;
            case keyCodes.CODE_KEY_ENTER:
                globalVariable.itemFocus.click();
                break;
        }
    }

    function focusCloseApp(index) {
        var closeAppDOM = document.querySelectorAll("#actionClose span");
        var nextClose = closeAppDOM[index];
        if(nextClose) {
            globalVariable.itemFocus = closeAppDOM[index];
            closeAppDOM[startIndex].classList.remove("active");
            nextClose.classList.add("active");
            startIndex = index;
        }
    }

    return window.closeApp = {
        compName: "closeApp",
        show: function(){
            globalVariable.allowAll = false;
            var contentBodyDOM = document.querySelector('.tv-body');
            contentBodyDOM.classList.add("close");
            contentBodyDOM.children[0].style.display = "none";
            contentBodyDOM.children[2].style.display = "block";
        },
        inactive: function() {
            var contentBodyDOM = document.querySelector('.tv-body');
            contentBodyDOM.classList.remove("close");
            contentBodyDOM.children[0].style.display = "block";
            contentBodyDOM.children[2].style.display = "none";
        },
        reactive: function(){
            focusCloseApp(startIndex);
        },
        keyHandle: function (event){
            keyHandle(event);
        },
        cancelClose: function(){
            globalVariable.allowAll = true;
            focusManager.active(globalVariable.storeComp);
        },
        okClose: function() {
            window.close();
        }
    }
});