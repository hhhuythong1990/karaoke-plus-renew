define(['text!./template.html', 'src/helper/mySocket', '../utils/utils.js', '../helper/keys'], 
    function(template, socket, utils, key) {
        var startIndex = 10;
        var stringInput = "";
    function keyHandle(event) {
        if(globalVariable.allowAll != true) return;
        switch(event.keyCode) {
            case keyCodes.CODE_KEY_UP:
                if(startIndex >= utils.CHAR_INDEX_30){
                    focusMenu(startIndex - utils.CHAR_RANGE_9);
                }else {
                    focusMenu(startIndex - utils.CHAR_RANGE_10);
                }
                break;
            case keyCodes.CODE_KEY_DOWN:
                if(startIndex >= utils.CHAR_INDEX_21 && startIndex < utils.CHAR_INDEX_30){
                    focusMenu(startIndex + utils.CHAR_RANGE_9);
                }else if(startIndex >= utils.CHAR_INDEX_30){
                    if(globalVariable.allowBottom == true){
                        var listKeys = document.getElementsByClassName("chars")[0].querySelectorAll('span');
                        listKeys[startIndex].classList.remove("active");
                        focusManager.active(songList);
                    }
                    
                }else {
                    focusMenu(startIndex + utils.CHAR_RANGE_10);
                }
                break;
            case keyCodes.CODE_KEY_RIGHT:
                focusMenu(startIndex + 1);
                break;
            case keyCodes.CODE_KEY_LEFT:
                var listKeys = document.getElementsByClassName("chars")[0].querySelectorAll('span');
                if(listKeys[startIndex].innerHTML == "1" || listKeys[startIndex].innerHTML == "x" 
                    || listKeys[startIndex].innerHTML == "q"){
                        listKeys[startIndex].classList.remove("active");
                        focusManager.active(leftMenu);
                } else{
                    focusMenu(startIndex - 1);
                }
                break;
            case keyCodes.CODE_KEY_ENTER:
                selectCharacter();
                break;
        }
    }

    function focusMenu(index) {
        var listKeys = document.getElementsByClassName("chars")[0].querySelectorAll('span');
        var nextCharacter = listKeys[index];
        if(nextCharacter) {
            globalVariable.itemFocus = listKeys[index];
            listKeys[startIndex].classList.remove("active");
            nextCharacter.classList.add("active");
            startIndex = index;
        }
    }
    

    function selectCharacter(){
        var charSelect = globalVariable.itemFocus.innerHTML;
        if(charSelect == "space" && stringInput.length > 0){
            stringInput += " ";
        }else if(charSelect == "clear"){
            stringInput = stringInput.slice(0, stringInput.length - 1);
        }else if(charSelect != "space" || charSelect != "clear"){
            stringInput += charSelect;
        }
        topPanel.searchValue(stringInput);
        switch(globalVariable.positionPage){
            case 1:
                if(stringInput == ""){
                    searchTube(utils.SEARCH_DEFAULT);
                }else {
                    searchTube(stringInput);
                }
                break;
            case 2:
                songList.searchBookMark(stringInput);
                break;
            case 3:
                songList.searchSequence(stringInput);
                break;
        }
    }

    function searchTube(contentSearch) {
        socket.emit("getList", { "keySearch":  contentSearch } );
    }

    return window.keyBoard = {
        compName: "keyBoard",
        show: function() {
            console.log("log show key-board");
            document.querySelector('.keyboard').innerHTML = template;
        },
        inactive: function() {
            topPanel.inactiveSearch();
        },
        hide: function(){
            document.querySelector('#leftPanel').style.display = 'none';
        },
        reactive: function (){
            topPanel.activeSearch();
            focusMenu(startIndex);
        },
        keyHandle: function(event){
            keyHandle(event);
        },
        clearSearchValue: function (){
            stringInput = "";
            document.getElementById("textSearch").value = "";
        },
        getStringSearch: function(){
            return stringInput;
        }
    }
})