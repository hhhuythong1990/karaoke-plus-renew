define([
    'text!./app.html',
    'src/helper/globalVariable',
    'src/utils/utils',
    'src/helper/focusManager',
    'src/helper/mySocket',
    'src/helper/keys', './top-panel/index' ,'./left-menu/index', './keyboards/index', './song-list/index', './video/index', './connection/index', './guide/index', './close-app/index'], 
    function(template, globalVariable, utils, focusManager, socket, key, topPanel, leftPanel, keyboard, songList, video, connection, guide, closeApp){
        window.addEventListener('keydown', function(e){
                event.preventDefault();
                event.stopPropagation();
                switch(event.keyCode) {
                    case key.CODE_KEY_SPACE:
                    case key.CODE_KEY_RED:
                        socket.emit("setToggleMenu");
                    break;
                    case key.CODE_KEY_GREEN:
                    case key.CODE_KEY_Z:
                        if(globalVariable.allowAll != true) return;
                        leftMenu.quickClick(0);
                        break;
                    case key.CODE_KEY_YELLOW:
                    case key.CODE_KEY_X:
                        if(globalVariable.allowAll != true) return;
                        leftMenu.quickClick(1);
                        break;
                    case key.CODE_KEY_BLUE:
                    case key.CODE_KEY_C:
                        if(globalVariable.allowAll != true) return;
                        leftMenu.quickClick(2);
                        break;
                    case key.CODE_KEY_BACKSPACE:
                    case key.CODE_KEY_RETURN:    
                        if(document.querySelector('.tv-body').classList.length > 1) return;
                        closeApp.show();
                        focusManager.active(closeApp);
                        return false;
                        break;
                    case key.CODE_KEY_PLAY:
                    case key.CODE_KEY_B:
                        video.pauseOrPlayVideo();
                        break;
                    case key.CODE_KEY_NEXT_TRACK:
                    case key.CODE_KEY_FAST_FORWARD:
                    case key.CODE_KEY_N:
                        video.nextSong();
                        break;
                    case key.CODE_KEY_PREV_TRACK:
                    case key.CODE_KEY_REWIND:
                    case key.CODE_KEY_V:
                        video.restartVideo();
                        break;
                    case 53:
                        location.reload();
                        break;
                    case 54:
                        console.log("log click debug");
                        if(!localStorage.getItem('debugOn')){
                            console.log("log debug ON");
                            video.showToastMessage(true, "DEBUG ON");
                            localStorage.setItem('debugOn', true);
                        }else {
                            console.log("log debug OFF");
                            video.showToastMessage(true, "DEBUG OFF");
                            localStorage.removeItem('debugOn');
                        }
                        break;
                }
        });
        function getDeviceId(){
            var deviceId = localStorage.getItem("deviceId");
            if(!deviceId){
                deviceId = randomDeviceId();
                localStorage.setItem("deviceId", deviceId);
            }
            return deviceId;
        }

        function randomDeviceId(){
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";      
            for (var i = 0; i < utils.RANGE_ID; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));      
            return text;
        }

        function toggleMenu(){
            console.log("start toggle-menu");
            socket.on("getToggleMenu", function (response) {
                console.log("toggle-menu response");
                if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                    var contentDOM = document.querySelector(".content");
                    if(globalVariable.contentShow == true){
                        contentDOM.style.opacity = 0;
                        globalVariable.contentShow = false;
                        globalVariable.allowAll = false;
                    }else {
                        contentDOM.style.opacity = 1;
                        globalVariable.contentShow = true;
                        globalVariable.allowAll = true;
                    }   
                    topPanel.toggleContent();
                }
            });
        }

        function hideIntroPage(){
            document.querySelector(".welcome-layer-1").style.zIndex = "0";
        }

    return {
        start: function(){
            console.log("log start app");
            socket.on('connect', function() {
                console.log("log connect");
                socket.emit("registerHost", { "deviceId": getDeviceId(), "platform": utils.PLATFORM, "keySearch": utils.SEARCH_DEFAULT });
            });
            socket.on('disconnect', function() {
                console.log("log disconnect");
                video.showToastMessage(true, "Bạn đang bị mất kết nối!");
            });
            socket.on('reconnect', function() {
                console.log("log reconnect");
                location.reload();
            });
            console.log("start load template");
            document.body.innerHTML = template;
            console.log("load template success");
            topPanel.show();
            leftPanel.show();
            keyboard.show();
            songList.show();
            video.show();
            setTimeout(function (){
                console.log("start timeout");
                toggleMenu();
                focusManager.active(songList);
                hideIntroPage();
                console.log("end timeout");
            }, 1000);
        }
    }
})