define(function() {
    var keyCodes = {
        "CODE_KEY_LEFT": 37,
        "CODE_KEY_UP": 38,
        "CODE_KEY_RIGHT": 39,
        "CODE_KEY_DOWN": 40,
        "CODE_KEY_ENTER": 13,
        "CODE_KEY_SPACE": 32,
        
        "CODE_KEY_BACKSPACE": 8,
        "CODE_KEY_Z": 90,
        "CODE_KEY_X": 88,
        "CODE_KEY_C": 67,
        "CODE_KEY_V": 86,
        "CODE_KEY_B": 66,
        "CODE_KEY_N": 78,

        "CODE_KEY_STOP": 413,
        "CODE_KEY_PAUSE": 19,
        "CODE_KEY_PLAY": 415,
        "CODE_KEY_FAST_FORWARD": 417,
        "CODE_KEY_REWIND": 412,

        "CODE_KEY_NEXT_TRACK": 425,
        "CODE_KEY_PREV_TRACK": 424,

        "CODE_KEY_RED": 403,
        "CODE_KEY_GREEN": 404,
        "CODE_KEY_YELLOW": 405,
        "CODE_KEY_BLUE": 406,
        "CODE_KEY_RETURN": 461,
    };
    return window.keyCodes = keyCodes;
})