define(['../../js/socket.io.js', '../utils/config'], function(io, config){
    return io(config.HOST_API, {"transports": ["websocket"], secure: true, reconnection: true});    
});