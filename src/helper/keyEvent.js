define(function() {
    
    var defaultHandle = function(e){
        this.console.log('keydown', e);
    }
    
    var currentHandle = defaultHandle;
    
    window.addEventListener('keydown', function(e){
        currentHandle(e);
    });

    return {
        hook: function(keyHandle) {
            currentHandle = keyHandle;
        },
        unhook: function() {
            currentHandle = defaultHandle;
        }
    }
})