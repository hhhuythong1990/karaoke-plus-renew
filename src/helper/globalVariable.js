define(function(){
    var globalVariable = {
        "contentShow": true,
        "itemEnter": null,
        "itemFocus": null,
        "positionPage": 1,
        "allowRight": true,
        "allowBottom": true,
        "allowAll": true,
        "storeComp": null,
    };
    return window.globalVariable = globalVariable;
});