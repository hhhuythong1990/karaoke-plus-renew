define(['./keyEvent'], function(keyEvent) {
    var current = null;
    return window.focusManager = {
        active: function(comp){
            if(comp.compName != "closeApp"){
                globalVariable.storeComp = comp;
            }
            
            if(current) {
                current.inactive();
            }
            current = comp;
            current.reactive();
            keyEvent.hook(comp.keyHandle);
            if(current.focusItem && typeof current.focusItem === "function"){
                current.focusItem();
            }
        }
    }
});