define(['text!./template.html', 'src/utils/utils', '../helper/keyEvent', '../helper/keys'], 
    function(template, utils, keyEvent, key){

        function createSwitchTopPanel () {
            var searchContentDOM = document.querySelector('.search-area');
            var element = document.createElement("div");

            var songName = document.createElement("div");
            songName.classList.add("song-name");
            var songText = document.createElement("p");
            songText.id = "songName";
            songText.textContent = video.videoPlaying();
            songName.appendChild(songText);

            var listWait = document.createElement("div");
            listWait.classList.add("list-wait");
            var songNumber = document.createElement("span");
            songNumber.id = "sequenceNum";
            songNumber.textContent = "Đang chờ: " + ((video.numberSequence()==undefined)?0:video.numberSequence()); 
            listWait.appendChild(songNumber);

            element.appendChild(songName);
            element.appendChild(listWait);
            searchContentDOM.removeChild(document.querySelector('.search-text'));
            searchContentDOM.appendChild(element);
        }
    
    return window.topPanel = {
        show: function() {
            console.log("log show top-panel");
            document.querySelector('.top-panel').innerHTML = template;
        },
        activeSearch: function() {
            document.getElementsByClassName("search-text")[utils.INDEX_0].classList.add("active");
        },
        inactiveSearch: function() {
            document.getElementsByClassName("search-text")[utils.INDEX_0].classList.remove("active");
        },
        searchValue: function(strSearch) {
            document.getElementById("textSearch").style.color = "#fff";
            document.getElementById("textSearch").value = strSearch;
        },
        toggleContent: function() {
            if(globalVariable.contentShow == false){
                document.querySelector(".circle").textContent = "Hiện menu";
                createSwitchTopPanel();
            }else {
                document.querySelector(".circle").textContent = "Ẩn menu";
                this.show();
            }
        },
        updateShowName: function(videoName){
            if(document.getElementById("songName")){
                document.getElementById("songName").textContent  = videoName;
            }
        },
        updateShowNum: function(sequenceNum){
            if(document.getElementById("sequenceNum")){
                document.getElementById("sequenceNum").textContent  = "Đang chờ: " + sequenceNum;
            }
        }
    }
})