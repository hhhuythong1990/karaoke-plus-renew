define(['text!./template.html', 'src/helper/mySocket', '../utils/utils.js', ''], function(template, socket, utils) {
    'use strict';

    function getConnectInfo () {
        socket.emit("getQrImage");
        socket.on("qrImage", function (response) {
            if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                document.querySelector('#textConnect').value = response.data.manualCode;
                document.querySelector('#imageConnect').src = "data:image/png;base64,"+ response.data.qrBase64
            }
        });
    }

    return window.connection = {
        show: function() {
            getConnectInfo();
            keyBoard.clearSearchValue();
            songList.positionPageChange(0);
            document.querySelector(".primary").innerHTML = template;
        }
    }
});