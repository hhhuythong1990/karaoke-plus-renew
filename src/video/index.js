define(['text!./template.html', 'src/helper/mySocket', 'src/utils/utils'], 
    function(template, socket, utils){
        var listSequence = [];
        var playingVideo = null;
        var timerCheckPlay;
        function loadVideo (strSource) {
            var timeReload = 0;
            var videoDOM = document.getElementById("video");
            videoDOM.style.opacity = utils.STR_1;
            // if(videoDOM.firstElementChild) {
            //     videoDOM.firstElementChild.src = url;
            //     videoDOM.firstElementChild.type = "video/"+typeVideo;
            // } else {
                // videoDOM.innerHTML = '<source src='+ url +' type="video/'+typeVideo+'"/>';
            // }
            
            videoDOM.innerHTML = strSource;
            videoDOM.oncanplay = function() {
                console.log("Can start playing video");
            };
            videoDOM.onstalled = function() {
                console.log("Media data is not available");
                videoDOM.load();
            };
            
            videoDOM.load();
            // videoDOM.play();
            showToastMessage(false, "");
            
            // if(timerCheckPlay != undefined){
            //     clearInterval(timerCheckPlay);
            // }
            // timerCheckPlay = setTimeout(function (){
            //     if(videoDOM.played.length == 0 && videoDOM.firstElementChild){
            //         // unloadVideo();
            //         setTimeout(function (){
            //             endVideo();
            //         },2000)
            //         videoDOM.style.opacity = utils.STR_0;
            //         showToastMessage(true, "Bài hát không thể phát. Vui lòng chọn bài khác!");
            //     }
            // }, 5000);
        }

        function endVideo() {
            var videoDOM = document.getElementById("video");
            videoDOM.style.opacity = utils.STR_0;
            if(listSequence.length > 0){
                if(listSequence[utils.INDEX_0]){                
                    socket.emit("removeSequence",{ "videoId": listSequence[0].videoDetail.id.videoId, "position": listSequence[utils.INDEX_0].videoDetail.originalPosition.toString() });
                    videoPlaying(listSequence[utils.INDEX_0].videoDetail);

                    var strSource = "";
                    var videoSources = listSequence[utils.INDEX_0].videoSource;
                    for (var videoSource in videoSources) {
                        strSource += '<source src='+ videoSources[videoSource].original_url +' type="video/'+ videoSources[videoSource].container +'"/>'
                    }
                    loadVideo(strSource);

                    // var videoSource = listSequence[utils.INDEX_0].videoSource;
                    // if(videoSource){
                    //     if(videoSource[utils.VIDEO_FORMAT_MP4_HD720]){
                    //         loadVideo(videoSource[utils.VIDEO_FORMAT_MP4_HD720].original_url, videoSource[utils.VIDEO_FORMAT_MP4_HD720].container);
                    //         videoDOM.style.opacity = utils.STR_1;    
                    //     } else if(videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM]){
                    //         loadVideo(videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM].original_url, videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM].container);
                    //         videoDOM.style.opacity = utils.STR_1;
                    //     } else if(videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM]){
                    //         loadVideo(videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM].original_url, videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM].container);
                    //         videoDOM.style.opacity = utils.STR_1;
                    //     }
                    // }                
                }           
            } else{
                unloadVideo();
                videoPlaying(null);
            } 
        }

        function listenSequence (){
            var videoDOM = document.getElementById("video");
            socket.on("sequenceAction", function (response) {  
                if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                    songList.updateStoreSequence(response.data);
                    if(response.data.type == utils.ACTION_ADD){
                        songList.addSequenceClass(response.data.addDetail.videoId);
                        if(!videoDOM.firstElementChild){                                                            
                            socket.emit("removeSequence", { videoId: response.data.detail[utils.INDEX_0].videoId, position: utils.STR_0 });
                            videoPlaying(response.data.addDetail.videoDetail);
                            var strSource = "";
                            var videoSources = response.data.detail[utils.INDEX_0].videoSource;
                            for (var videoSource in videoSources) {
                                strSource += '<source src='+ videoSources[videoSource].original_url +' type="video/'+ videoSources[videoSource].container +'"/>'
                            }
                            loadVideo(strSource);

                            // var videoSource = response.data.detail[utils.INDEX_0].videoSource;
                            // if(videoSource){
                            //     if(videoSource[utils.VIDEO_FORMAT_MP4_HD720]){
                            //         videoDOM.style.opacity = utils.STR_1;
                            //         loadVideo(videoSource[utils.VIDEO_FORMAT_MP4_HD720].original_url, videoSource[utils.VIDEO_FORMAT_MP4_HD720].container);
                            //     } else if(videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM]){
                            //         videoDOM.style.opacity = utils.STR_1;
                            //         loadVideo(videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM].original_url, videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM].container);
                            //     } else if(videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM]){
                            //         videoDOM.style.opacity = utils.STR_1;
                            //         loadVideo(videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM].original_url, videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM].container);
                            //     }
                            // }
                            listSequence = [];
                        } else{
                            if(globalVariable.positionPage == 3){
                                songList.addSequenceElement();
                                songList.resetAll();
                            }
                            listSequence.push(response.data.detail[utils.INDEX_0]);
                        }
                    }else if(response.data.type == utils.ACTION_REMOVE) {
                        if(globalVariable.positionPage == 1){
                            songList.removeSequenceClass(response.data.removeDetail.videoId);
                        }else if(globalVariable.positionPage == 3){
                            songList.updateElementSequence(response.data.removeDetail.videoId, response.data.detail);
                            songList.resetAll();
                        }
                        if(listSequence.length == 0){
                            listSequence = [];                        
                        } else {
                            listSequence = response.data.detail;
                        }
                    }
                    topPanel.updateShowNum(listSequence.length);
                }      
            });
        }

        function listenPriority () {
            socket.on("prioritySort", function (response) {
                if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                    listSequence = response.data.detail;
                    if(globalVariable.positionPage == 3){
                        songList.updateStoreSequence(response.data);
                        songList.addSequenceElement();
                        songList.resetAll();
                    }
                }
            }); 
        }

        function listenBookMark(){
            socket.on("bookMarkAction", function (response) {
                if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                    songList.updateStoreBookMark(response.data);
                    if(response.data.type == utils.ACTION_ADD){
                        if(globalVariable.positionPage == 1){
                            songList.addBookMarkClass(response.data.detail[utils.INDEX_0].videoId);
                        }else if(globalVariable.positionPage == 2){
                            songList.addBookMarkElement();
                            songList.resetAll();
                        }
                    }
                    if(response.data.type == utils.ACTION_REMOVE) {
                        songList.removeSongElement(response.data.removeDetail.videoId);
                        if(globalVariable.positionPage == 2){
                            songList.resetAll();
                        }
                    }
                }
            });        
        }

        function listenMobileSequence(){
            socket.on("mobileSequenceVideo", function (response) {
                if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                    if(response.data.type == utils.SEQUENCE_ACTION_ADD_MOBILE){
                        socket.emit("addSequence", 
                            { "videoId": response.data.detail[utils.INDEX_0].videoId, "videoDetail": JSON.stringify(response.data.detail[utils.INDEX_0].videoDetail) }
                        );
                    }else {
                        socket.emit("removeSequence", 
                            { "videoId": response.data.sequenceDetail.videoId, "position": response.data.sequenceDetail.videoDetail.originalPosition.toString() }
                        );
                    }                
                }
            });
        }

        function listenSelectVideo(){
            var videoDOM = document.getElementById("video");
            socket.on("playVideo", function (response) {
                if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                    videoPlaying(response.data.videoDetail);
                    var strSource = "";
                    var videoSources = response.data.videoSource;
                    for (var videoSource in videoSources) {
                        strSource += '<source src='+ videoSources[videoSource].original_url +' type="video/'+ videoSources[videoSource].container +'"/>'
                    }
                    loadVideo(strSource);
                    // if(!videoDOM.firstElementChild){
                    //     videoDOM.style.opacity = utils.STR_1;
                    //     if(videoSource[utils.VIDEO_FORMAT_MP4_HD720]){
                    //         loadVideo(videoSource[utils.VIDEO_FORMAT_MP4_HD720].original_url, videoSource[utils.VIDEO_FORMAT_MP4_HD720].container);
                    //     } else if(videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM]){
                    //         loadVideo(videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM].original_url, videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM].container);
                    //     } else if(videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM]){
                    //         loadVideo(videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM].original_url, videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM].container);
                    //     } 
                    // }else {
                    //     if(videoSource[utils.VIDEO_FORMAT_MP4_HD720]){
                    //         loadVideo(videoSource[utils.VIDEO_FORMAT_MP4_HD720].original_url, videoSource[utils.VIDEO_FORMAT_MP4_HD720].container);
                    //     } else if(videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM]){      
                    //         loadVideo(videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM].original_url, videoSource[utils.VIDEO_FORMAT_MP4_MEDIUM].container);
                    //     } else if(videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM]){
                    //         loadVideo(videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM].original_url, videoSource[utils.VIDEO_FORMAT_WEBM_MEDIUM].container);
                    //     }                   
                    // }
                }                
            });
        }

        function prioritySortMobile () {
            socket.on("mobilePriorityVideo", function (response) {
                if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                    socket.emit("priorityVideo", { "videoId": response.data.detail[utils.INDEX_0].videoId, "position": response.data.detail[utils.INDEX_0].position.toString() });
                }            
            });
        }

        function registerVideoEvents() {
            var videoDOM = document.getElementById("video");
            videoDOM.addEventListener("ended", function () { endVideo() });
        }

        function unloadVideo() {
            var videoDOM = document.getElementById("video");
            if(videoDOM.firstElementChild) {
                videoDOM.firstElementChild.remove();
            }
            videoDOM.load();
        }

        function pauseOrPlayVideo(){
            var videoDOM = document.getElementById("video");
            if(videoDOM.firstElementChild) {
                if (videoDOM.paused) {
                    videoDOM.play(); 
                }else { 
                    videoDOM.pause(); 
                } 
            }
        }

        function pauseVideo(){
            var videoDOM = document.getElementById("video");
            if(videoDOM.firstElementChild) {
                if (videoDOM.paused) {
                    videoDOM.play(); 
                }
            }
        }

        function listenMobileSelectVideo (){
            socket.on("mobileSelectVideo", function (response){
                if(response.status == utils.RESPONSE_STATUS_SUCCESS){
                    socket.emit("selectVideo", { "videoId": response.data.detail[utils.INDEX_0].videoId, "videoDetail": JSON.stringify(response.data.detail[utils.INDEX_0].videoDetail) });
                }            
            });
        }

        function videoPlaying(videoData){
            if(videoData){
                playingVideo = videoData.snippet.title;
            }else {
                playingVideo = null;
            }
            topPanel.updateShowName(playingVideo);
            return playingVideo;
        }

        function showToastMessage(status, strMsg) {
            var toastDOM = document.querySelector(".text-info");
            if(status == true){
                toastDOM.style.display = "block";
            }else if(status == false){
                toastDOM.style.display = "none";
            }
            toastDOM.firstChild.textContent = strMsg;
        }

        return window.video = {
            show: function() {
                console.log("log show video");        
                document.getElementById('main-body').innerHTML += template;
                registerVideoEvents();
                listenSelectVideo();
                listenSequence();
                listenBookMark();
                listenPriority();
                listenMobileSelectVideo();
                listenMobileSequence();
                prioritySortMobile();
            },
            loadVideo: function(urlVideo){
                loadVideo(urlVideo);
            },
            numberSequence: function () {
                return listSequence.length;
            },
            videoPlaying: function (videoData) {
                return playingVideo;
            },
            pauseOrPlayVideo: function (){
                pauseOrPlayVideo();
            },
            pauseVideo: function() {
                pauseVideo();
            },
            nextSong: function() {
                endVideo();
            },
            restartVideo: function(){
                var videoDOM = document.getElementById("video");
                if(videoDOM.firstElementChild) {
                    videoDOM.currentTime = 0;
                }
            },
            showToastMessage: function(status, msg){
                showToastMessage(status, msg);
            }
        }
})