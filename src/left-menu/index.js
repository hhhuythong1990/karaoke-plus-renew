define(['text!./template.html', '../utils/utils.js', '../helper/keyEvent'], 
    function(template, utils, keyEvent) {
        var startIndex = 0;
        var clickIndex = 0;
    function keyHandle(event) {
        if(globalVariable.allowAll != true) return;
        switch(event.keyCode) {
            case keyCodes.CODE_KEY_UP:
                focusMenu(startIndex - 1);
                break;
            case keyCodes.CODE_KEY_DOWN:
                focusMenu(startIndex + 1);
                break;
            case keyCodes.CODE_KEY_RIGHT:
                if(startIndex < 3 && globalVariable.allowRight == true){
                    focusManager.active(songList);
                }
                break;
            case keyCodes.CODE_KEY_ENTER:
                if(startIndex < 3){
                    clickIndex = startIndex;
                    globalVariable.allowRight = true;
                }else {
                    globalVariable.allowRight = false;
                }
                globalVariable.itemFocus.click();
                break;
            
        }
    }

    function focusMenu(index) {
        var mainMenuDOM = document.querySelector(".main-menu");
        var nextMenu = mainMenuDOM.children[index];
        if(nextMenu) {
            globalVariable.itemFocus = mainMenuDOM.children[index];
            mainMenuDOM.children[startIndex].classList.remove("hovered");
            nextMenu.classList.add("hovered");
            startIndex = index;
        }
    }

    function firstShow(){
        var mainMenuDOM = document.querySelector(".main-menu");
        if(mainMenuDOM){
            mainMenuDOM.children[startIndex].classList.remove("actived");
            mainMenuDOM.children[startIndex].classList.remove("hovered");
            mainMenuDOM.children[clickIndex].classList.add("actived");
            mainMenuDOM.children[clickIndex].classList.remove("hovered");
        }
    }

    return window.leftMenu = {
        compName: "leftMenu",
        show: function() {
            console.log("log show left-menu");
            document.querySelector('.content .main-menu').innerHTML = template;
            firstShow();
        },
        inactive: function() {
            firstShow();
        },
        keyHandle: function (event){
            keyHandle(event);
        },
        reactive: function (){
            var mainMenuDOM = document.getElementsByClassName("main-menu")[utils.INDEX_0];
            mainMenuDOM.children[clickIndex].classList.remove("actived");
            focusMenu(clickIndex);
        },
        showConnectPage: function (){
            showConnectPage();
        },
        quickClick: function(index) {
            var mainMenuDOM = document.querySelector(".main-menu");        
            mainMenuDOM.children[startIndex].classList.remove("actived");
            mainMenuDOM.children[clickIndex].classList.remove("actived");
            mainMenuDOM.children[index].click();
            clickIndex = index;
            focusManager.active(leftMenu);
            if(document.querySelector(".chars .active")){
                var keyboardSelected = document.querySelector(".chars .active");
                keyboardSelected.classList.remove("active");
            }
        }
    }
})