/*
https://www.npmjs.com/package/vue-resource
https://laracasts.com/discuss/channels/vue/processing-ajax-requests-from-vuejs?page=1

localStorage.platform = 'Samsung';
localStorage.env = 'dev';
localStorage.deviceModel = 'UA43MU6101';
localStorage.eplcount = 0;
*/

// 'use strict';
require.config({
    config: {
        text: {
            useXhr: function (url, protocol, hostname, port) {
                return true;
            }
        }
    },
	paths: {
        "text": "js/html",
    }
    // shim: {
    //     'lodash': {
    //         exports: '_'
    //     },
    //     'firebase': {
    //         exports: "firebase"
    //     },
    //     'firestore': {
    //         deps: ['firebase']
    //     }
    // }
});

require(['src/app'], function (app) {
    app.start();
});

